class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }

    get age() {
        return this._age;
    }

    get salary() {
        return this._salary;
    }

    set name(newName) {
        this._name = newName;
    }

    set age(newAge) {
        this._age = newAge;
    }

    set salary(newSalary) {
        this._salary = newSalary;
    }
}


console.log(Employee);



class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }

    get salary() {
        return this._salary * 3;
    }

    get lang() {
        return this._lang;
    }

    set lang(newLang) {
        this._lang = newLang;
    }
}

console.log(Programmer);



const programmer1 = new Programmer("Andrey", 31, 50000, ["JavaScript", "Python"]);
const programmer2 = new Programmer("Slava", 29, 40000, ["JavaScript", "C++"]);
const programmer3 = new Programmer("Petro", 41, 70000, ["C#", "Java"]);

// Виведення даних у консоль
console.log("Програмісти:");
console.log(`1. Ім'я: ${programmer1.name}, Зарплата: ${programmer1.salary}, Мови: ${programmer1.lang.join(', ')}`);
console.log(`2. Ім'я: ${programmer2.name}, Зарплата: ${programmer2.salary}, Мови: ${programmer2.lang.join(', ')}`);
console.log(`3. Ім'я: ${programmer3.name}, Зарплата: ${programmer3.salary}, Мови: ${programmer3.lang.join(', ')}`);
